from flask import Flask, request
import json
app = Flask(__name__)


@app.route('/patients', methods=['GET'])
def all_patients_handler():
    if request.method == 'GET':
        return get_patient_list()


@app.route('/interventions', methods=['GET'])
def all_interventions_handler():
    if request.method == 'GET':
        return get_intervention_list()


# responses are hard-coded

def get_patient_list():
    response_dict = {
        "status": "OK",
        "data": [{"name": "Bergliot Tarek"},
                 {"name": "Jagienka Tineke"},
                 {"name": "Prem Anacletus"}]
        }
    return json.dumps(response_dict)


def get_intervention_list():
    response_dict = {
        "status": "OK",
        "data": [{"name": "Simple"},
                 {"name": "Medium"},
                 {"name": "Intensive"}]
        }
    return json.dumps(response_dict)


def run_app():
    app.run(host='0.0.0.0', port=7560)


run_app()

